#!/usr/bin/env python2
import sys
import subprocess
import math
from scipy import stats
from numpy import array, mean

# read in all the cliqmap files
# split based on CLIQUE statements
# whatever list you get, pass it to superimposer.bin, obtain RMSD of superimposition from stdout (OS MODULE)
# check stderr for errors?
# read list of scores from corresponding .cliqscores file
# list of rmsd, list of scores - correlate? MCC?

# read in the cliqmap file

def mcc_finder(tp,tn,fp,fn):
    numerator = float(((tp*tn)-(fp*fn)))
    denom_sq = float((tp+fp)*(tp+fn)*(tn+fp)*(tn+fn))
    # print("num and denom_sq are " + str(numerator) + " and " + str(denom_sq))
    # print("denom is " + str(math.sqrt(denom_sq)))
    mcc = numerator/math.sqrt(denom_sq)
    return mcc

def getFileNameWithoutExtension(path):
    # this function does exactly what it's name says (duh!)
    return path.split('\\').pop().split('/').pop().rsplit('.', 1)[0]


def rmsfvsscore(cliqmapfile, cliqwisescoresfile):
    with open(cliqmapfile, 'r') as cliqmapobj:
        cliqmapf = cliqmapobj.read()

    with open(cliqmapfile, 'r') as cliqmapobj:
        cliqmaplines = cliqmapobj.readlines()
        cliqmapsucconly = [j for i,j in enumerate(cliqmaplines[:-1]) if cliqmaplines[i+1].startswith('CLIQUE')]
    cliqmapdict = {(i.split()[0])+"_"+(i.split()[1]): i for i in cliqmapsucconly}
    cliqmaps = cliqmapf.split("CLIQUE")[:-1]
    # print(cliqmaps[-5:])

    succID = getFileNameWithoutExtension(cliqmapfile).split("_")[0]
    succcliqfilepath = "modsuccessstars/" + succID + "_mod.cliq"

    with open(succcliqfilepath, 'r') as cliqpathobj:
        cliqpathf = cliqpathobj.readlines()
    cliqdict = {(i.split()[0])+"_"+(i.split()[1]): i for i in cliqpathf}

    with open(cliqwisescoresfile, 'r') as scoresfobj:
        scoresfile = scoresfobj.readlines()

    scoresdict={}
    for smline in scoresfile:
        scoresdict[int(smline.split()[0])] = smline.split()[1]

    # sys.stdout.write("Reading all files done. Superimposition to find rmsf is in progress...\n")
    finalvector = []        # of the form [(rmsf, score)]
    for i in cliqmaps:
        # print(str(i).lstrip())
        thecentre = str(i).lstrip().split()[1]
        succthecentre = str(i).lstrip().split('\n')[1].split()[1]
        # print(succthecentre)
        thestring = "printf \"" + str(i).lstrip() + "\""
        # print(thestring)
        printfcall = subprocess.Popen(thestring, shell=True, stdout=subprocess.PIPE)
        # superimposecall = ("printf", str(i).lstrip(), "|", "superimposer.bin", "-q", "modpredecgpdbs", "-t", "modsuccessgpdbs")
        # superimposecall = ("superimposer.bin", "-q", "modpredecgpdbs", "-t", "modsuccessgpdbs")
        superimposecall = "superimposer.bin -q modpredecgpdbs -t modsuccessgpdbs -r 8"
        # print(superimposecall)
        # printfout = printfcall.stdout.read()
        # print("printfout is ", printfout)
        popen = subprocess.Popen(superimposecall, stdin=printfcall.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        popen.wait()
        output = popen.stdout.read()
        error = popen.stderr.read()
        # print('output is \n' + output + "\n")
        # print('error is ' + error + "\n")
        # print("rmsd is "+ output.split()[-1])
        indexofcliqueinpredec = int(str(i).strip().split()[1])+1
        # print(error)
        if error.strip() != "":
            print("error during superimposition found")
            sys.exit(1)
        # print(str(indexofcliqueinpredec) + "\t" + rmsf + "\t" + scoresdict[indexofcliqueinpredec])
        rmsf = output.split()[-1]
        thescore = scoresdict[indexofcliqueinpredec]

        #find whether the clique in predec had formed in successor
        dictkey = succID + "_mod_" + succthecentre
        # print(i)
        if (dictkey in cliqdict.keys()) and cliqmapdict[dictkey] == cliqdict[dictkey]:
            matched = 1
            # print(dictkey, cliqmapdict[dictkey])
        else:
            matched = 0

        finalvector.append((int(thecentre), float(rmsf), float(thescore), matched))
    # sys.stdout.write("\rSuperimpositions complete\n")

    return finalvector

def correlationfinder(cliqmapfile, cliqwisescoresfile, outfobj):
    with open(cliqmapfile, 'r') as cliqmapobj:
        cliqmapf = cliqmapobj.read()
    # outfobj.write("blah blah\n")
    cliqmaps = cliqmapf.split("CLIQUE")[:-1]
    # print(cliqmaps[-5:])

    with open(cliqwisescoresfile, 'r') as scoresfobj:
        scoresfile = scoresfobj.readlines()

    scoresdict={}
    for smline in scoresfile:
        scoresdict[int(smline.split()[0])] = smline.split()[1]

    # sys.stdout.write("Reading all files done. Superimposition to find rmsf is in progress...\n")
    finalvector = []        # of the form [(rmsf, score)]
    for i in cliqmaps:
        # print(str(i).lstrip())
        thestring = "printf \"" + str(i).lstrip() + "\""
        # print(thestring)
        printfcall = subprocess.Popen(thestring, shell=True, stdout=subprocess.PIPE)
        # superimposecall = ("printf", str(i).lstrip(), "|", "superimposer.bin", "-q", "modpredecgpdbs", "-t", "modsuccessgpdbs")
        # superimposecall = ("superimposer.bin", "-q", "modpredecgpdbs", "-t", "modsuccessgpdbs")
        superimposecall = "superimposer.bin -q modpredecgpdbs -t modsuccessgpdbs -r 8"
        # print(superimposecall)
        # printfout = printfcall.stdout.read()
        # print("printfout is ", printfout)
        popen = subprocess.Popen(superimposecall, stdin=printfcall.stdout, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        popen.wait()
        output = popen.stdout.read()
        error = popen.stderr.read()
        # print('output is \n' + output + "\n")
        # print('error is ' + error + "\n")
        # print("rmsd is "+ output.split()[-1])
        indexofcliqueinpredec = int(str(i).strip().split()[1])+1
        # print(error)
        if error.strip() != "":
            print("error during superimposition found")
            sys.exit(1)
        # print(str(indexofcliqueinpredec) + "\t" + rmsf + "\t" + scoresdict[indexofcliqueinpredec])
        rmsf = output.split()[-1]
        thescore = scoresdict[indexofcliqueinpredec]
        finalvector.append((float(rmsf), float(thescore)))
    # sys.stdout.write("\rSuperimpositions complete\n")
    finalvector = array(finalvector)
    # print(finalvector[:,0])
    # pcc,pval = stats.pearsonr(finalvector[:,0], finalvector[:,1])
    # print(pcc, pval)

    rmsfcutoff = 1.0
    scorecutoff = 2.0
    themeanrmsf = mean(finalvector[:,0])
    themaxrmsf = max(finalvector[:,0])
    # print("meanrmsf, maxrmsf")
    # print(themeanrmsf, themaxrmsf)
    tp=0
    fp=0
    tn=0
    fn=0
    for count, i in enumerate(finalvector):
        if i[0] < rmsfcutoff and i[1] < scorecutoff:
            # print("tp found")
            tp = tp+1
        elif i[0] > rmsfcutoff and i[1] < scorecutoff:
            # print("fp found")
            fp = fp+1
        elif i[0] > rmsfcutoff and i[1] > scorecutoff:
            # print("tn found")
            tn = tn+1
        elif i[0] < rmsfcutoff and i[1] > scorecutoff:
            # print("fn found")
            fn = fn+1
            # print(count+1, i)
    # print("tp,fp,tn,fn")
    # print(tp,fp,tn,fn)
    accuracy = round(float(tp + tn)/(tp+fp+tn+fn),3)
    recall = round(float(tp)/(tp+fn),3)
    # print("accuracy, recall")
    # print(accuracy,recall)
    try:
        thismcc = mcc_finder(tp,tn,fp,fn)
    except Exception as e:
        thismcc = 0
    # print("mcc is ", thismcc)
    # print(thismcc, accuracy, recall)
    outfobj.write(str(tp)+"\t"+str(fp)+"\t"+str(tn)+"\t"+str(fn)+"\t"+str(accuracy)+"\t"+str(recall)+"\t"+str(thismcc)+"\t"+cliqmapfile+"\n")

if __name__ == "__main__":
    with open(sys.argv[1], 'r') as thefobj:
        thearguments = thefobj.readlines()
        # print(thearguments)
    # with open('correlationfinder.out', 'w') as outfileobj:
        # outfileobj.write("TP\tFP\tTN\tFN\tAcc\tRecall\tMCC\tFile\n")
        # for i in thearguments:
            # j = i.strip().split()[0]
            # k = i.strip().split()[1]
            # print("processing "+i+"  "+j+"  "+k)
            # correlationfinder(j, k, outfileobj)

    with open('rmsfvsscore.out', 'w') as rmsffobj:
        rmsffobj.write('{:<6}{:<6}{:<6}{:<7}{:<7}{:<7}'.format("succ", "pred", "cen", "rmsf", "score", "matched") + "\n")
    for i in thearguments:
        j = i.strip().split()[0]        # cliqmap file
        k = i.strip().split()[1]        # scores file
        print("processing "+j+"  "+k)
        finalvec = rmsfvsscore(j, k)
        specificrmsfvsscoreout = "rmsfvsscore/" + getFileNameWithoutExtension(j) + "_rmsfvsscore.out"
        with open(specificrmsfvsscoreout, 'w') as rmsffobj:
            rmsffobj.write('{:<6}{:<6}{:<6}{:<7}{:<7}{:<7}'.format("succ", "pred", "cen", "rmsf", "score", "matched") + "\n")

        scoresf_sansext = getFileNameWithoutExtension(k)
        successorID = scoresf_sansext.split("_")[0]
        predecessorID = scoresf_sansext.split("_")[2]
        with open('rmsfvsscore.out', 'a') as rmsffobj:
            for i in finalvec:
                rmsffobj.write('{:<6}{:<6}{:<6}{:<7}{:<7}{:<7}'.format(successorID, predecessorID, str(i[0]), str(round(i[1], 4)), str(round(i[2], 4)), str(i[3])) + "\n")
        with open(specificrmsfvsscoreout, 'a') as rmsffobj:
            for i in finalvec:
                rmsffobj.write('{:<6}{:<6}{:<6}{:<7}{:<7}{:<7}'.format(successorID, predecessorID, str(i[0]), str(round(i[1], 4)), str(round(i[2], 4)), str(i[3])) + "\n")
        print("finished "+j +" "+k)
