#!/bin/bash

cd /storage/swastik/workingdir/obsoletepdbrefinement/scoringnr30pdbs_20180805; ls *.cliqwisescores > correlationfindercommands.txt; mv correlationfindercommands.txt /storage/swastik/obsoletepdbrefinement/nr30; cd /storage/swastik/obsoletepdbrefinement/nr30

awk -F_8_ '{print $1}' correlationfindercommands.txt > correlationfindercommands.txt2

#$ head correlationfindercommands.txt2
#1c75_from_1b7c_
#1eu1_from_1cxs_
#1eu1_from_1cxt_
#1iqz_from_1fxb_
#1iqz_from_2fxb_
#1nth_from_1l2r_
#1o2d_from_1j5r_
#1ock_from_1gr
#1vqq_from_1mwx_
#1xff_from_1gdo_

sed 's/^/modsuccessstars\//g' correlationfindercommands.txt2| sed 's/_from_/_mod_from_/g'|sed 's/$/_mod/g'|sed 's/$/\.cliqmap/g' > correlationfindercommands.txt2_2
sed 's/^/cliqwisescoresdir\//g' correlationfindercommands.txt2| sed 's/$/_8_20_2_10_1000.cliqwisescores/g' > correlationfindercommands.txt2_3

paste -d" " correlationfindercommands.txt2_2 correlationfindercommands.txt2_3 > correlationfindercommands.txt

#sed -i 's/^/\.\/correlationfinder.py /g' correlationfindercommands.sh

rm correlationfindercommands.txt2*

#./correlationfinder.py correlationfindercommands.txt > correlationfinder.log 2>&1
