#!/usr/bin/env python3
import sys

# read cliqmap file and create a dictionary of centre of predecessor mapped to centre of successor
# read cliqwisesuccscoresfile and create a dictionary of centre of successor star and score of the star
def getFileNameWithoutExtension(path):
    # this function does exactly what it's name says (duh!)
    return '.'.join(path.split('\\').pop().split('/').pop().split('.')[:-1])


with open(sys.argv[1], 'r') as argumentfileobj:
    thearguments = argumentfileobj.readlines()


for theargument in thearguments:
    print(theargument)
    cliqmapdict = {}
    cliqmapfilepath = theargument.split()[0]
    succcliqscorespath = theargument.split()[1]

    with open(cliqmapfilepath, 'r') as cliqmapobj:
        cliqmapf = cliqmapobj.readlines()

    with open(succcliqscorespath, 'r') as succscoresobj:
        succscoresf = succscoresobj.readlines()

    cliqmapdict = {j.split()[1]:cliqmapf[i+1].split()[1] for i,j in enumerate(cliqmapf[:-2]) if cliqmapf[i+2].startswith('CLIQUE')}
    # print(cliqmapdict)
    cliqscoresdict = {j.split()[0]:j.split()[1] for j in succscoresf}
    # print(cliqscoresdict)

    thefilebasename = getFileNameWithoutExtension(cliqmapfilepath)
    thermsdvsscorefilepath = "rmsfvsscore/"+thefilebasename+"_rmsfvsscore.out"
    with open(thermsdvsscorefilepath, 'r') as tomodifyobj:
        tomodifyfile = tomodifyobj.readlines()

    therawlist = [i.split() for i in tomodifyfile[1:]]
    for theindex,i in enumerate(therawlist): 
        thesuccscore = cliqscoresdict[str( int(cliqmapdict[i[2]])+1 )]
        therawlist[theindex].append(str(round(float(thesuccscore), 4)))
        if float(i[4]) < float(thesuccscore):
            therawlist[theindex].append('0')
        else:
            therawlist[theindex].append('1')


    # print(therawlist)
    with open("rmsfvsscore/" + thefilebasename + "_predvssucc.out", 'w') as outfobj:
        outfobj.write('{:<7}{:<7}{:<9}{:<7}{:<9}{:<9}{:>7}{:>7}'.format('succ', 'pred', 'pred-cen', 'rmsf', 'pr-score', 'su-score', 'matched?', 'better?') + "\n")
        for i in therawlist:
            outfobj.write('{:<7}{:<7}{:<9}{:<7}{:<9}{:<9}{:>7}{:>7}'.format(i[0], i[1], i[2], i[3], i[4], i[6], i[5], i[7]) + "\n")
